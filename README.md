# peertube-xp

Un déploiement très basique de Peertube via Docker.

## howto

```shell
cp .env.example .env
$editor .env
docker-compose up -d
```

## env from example to exemple

```diff
--- .env.example        2019-10-11 02:12:54.106793054 +0200
+++ .env        2019-10-11 02:12:02.251430337 +0200
@@ -1,6 +1,6 @@
 PEERTUBE_DB_USERNAME=postgres_user
 PEERTUBE_DB_PASSWORD=postgres_password
-PEERTUBE_WEBSERVER_HOSTNAME=domain.tld
+PEERTUBE_WEBSERVER_HOSTNAME=peertest.garexp.org
 PEERTUBE_WEBSERVER_PORT=443
 PEERTUBE_WEBSERVER_HTTPS=true
 # If you need more than one IP as trust_proxy
@@ -10,10 +10,10 @@
 #PEERTUBE_SMTP_PASSWORD=
 PEERTUBE_SMTP_HOSTNAME=postfix
 PEERTUBE_SMTP_PORT=25
-PEERTUBE_SMTP_FROM=noreply@domain.tld
+PEERTUBE_SMTP_FROM=peertube@garexp.org
 PEERTUBE_SMTP_TLS=false
 PEERTUBE_SMTP_DISABLE_STARTTLS=false
-PEERTUBE_ADMIN_EMAIL=admin@domain.tld
+PEERTUBE_ADMIN_EMAIL=tom+peertest@garexp.org
 # /!\ Prefer to use the PeerTube admin interface to set the following configurations /!\
 #PEERTUBE_SIGNUP_ENABLED=true
 #PEERTUBE_TRANSCODING_ENABLED=true
 ```
